## Prerequisites
Install node and npm from the [NodeJS website](https://nodejs.org/en/download/)

## Setup
Install packages by entering the following npm command in a terminal window (command window in Windows):
npm install

Compile and run by entering the following npm command in a terminal window (command window in Windows):
npm start