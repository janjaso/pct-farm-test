import {Farm} from './../entities/farm';
import {Farmfield} from './../entities/farmfield';
//Assume this comes from a HTTP GET API ENDPOINT SOMEWHERE.
export var FARMS: Farm[] = [
    {
        "id": 1,
        "name": "Midway",
        "owner": "John",
        "phone": "0400111222",
        "totalarea": 48.23,
        "averagelat": -26.93000,
        "averagelong": 145.00000,
        "fields": [
            {
                "id": 1,
                "name": "South",
                "area": 27.3,
                "crop": "Strawberries",
                "lat": -26.93000,
                "long": 145.00000,
            }
        ],
    },
    {
        "id": 2,
        "name": "Xing",
        "owner": "Michael",
        "phone": "0400222333",
        "totalarea": 109.40,
        "averagelat": -36.66667,
        "averagelong": 142.00000,
        "fields": [
            {
                "id": 1,
                "name": "House",
                "area": 18.7,
                "crop": "Cotton",
                "lat": -35,
                "long": 142,
            },
            {
                "id": 2,
                "name": "Lake",
                "area": 12.8,
                "crop": "Soybeans",
                "lat": -35,
                "long": 143,
            },
            {
                "id": 3,
                "name": "West",
                "area": 77.9,
                "crop": "Cotton",
                "lat": -37,
                "long": 141,
            }
        ],
    },
    {
        "id": 3,
        "name": "Empty Fields",
        "owner": "No Farm Fields Guy",
        "phone": "0400111999",
        "totalarea": 48.23,
        "averagelat": -26.93000,
        "averagelong": 145.00000,
        "fields": [],
    },
];