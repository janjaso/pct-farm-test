import {Injectable} from 'angular2/core';

import {Farm} from './../entities/farm';
import {FARMS} from './../mock-data/mock-farms';

@Injectable()
export class FarmService {
    getFarms() {
        return Promise.resolve(FARMS);
    }

    getSlowFarms() {
        return new Promise<Farm[]>(resolve =>
            setTimeout(()=>resolve(FARMS), 2000) // 2s delay
        );
    }
}