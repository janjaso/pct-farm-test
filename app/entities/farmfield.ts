export class Farmfield {
    id: number;
    name: string;
    area: number;
    crop: string;
    lat: number;
    long: number;
}