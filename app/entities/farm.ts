import {Farmfield} from './farmfield';
export class Farm {
    id: number;
    name: string;
    owner: string;
    phone: string;
    totalarea: number;
    averagelat: number;
    averagelong: number;
    fields: Farmfield[];
}

