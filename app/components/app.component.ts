import {Component, OnInit} from 'angular2/core';

import {Farm} from './../entities/farm';
import {FarmDetailComponent} from './farm-detail.component';
import {FarmService} from './../services/farm.service';

@Component({
    selector: 'my-app',
    styles:[`
      .selected {
        background-color: #CFD8DC !important;
        color: white;
      }
      table {
        border: solid 1px;
        border-spacing: 5px;
      }
    `],
    template:`
<h1>{{title}}</h1>
<h2>My Farms</h2>
<table >
    <tbody>
    <tr>
        <th>Name</th>
        <th>Owner</th>
        <th>Phone</th>
        <th>Total Area</th>
        <th>Average Lat</th>
        <th>Average Long</th>
        <th></th>
    </tr>
    <tr *ngFor="#farm of farms"
        [class.selected]="farm === selectedFarm"
        (click)="onSelect(farm)">
        <th>{{farm.name}}</th>
        <th>{{farm.owner}}</th>
        <th>{{farm.phone}}</th>
        <th>{{farm.totalarea}}</th>
        <th>{{farm.averagelat}}</th>
        <th>{{farm.averagelong}}</th>
        <td><input type="button" value="Del"></td>
    </tr>
    </tbody>
</table>
<my-farm-detail [farm]="selectedFarm"></my-farm-detail>`,
    directives: [FarmDetailComponent],
    providers: [FarmService]
})

export class AppComponent implements OnInit {
    title = 'PCT Farm Test';
    farms: Farm[];
    selectedFarm: Farm;

    constructor (private farmService: FarmService) {}

    getFarms() {
        this.farmService.getSlowFarms().then(farms => this.farms = farms);
    }

    ngOnInit() {
        this.getFarms();
    }

    onSelect(farm: Farm) { this.selectedFarm = farm; }
}
