import {Component, Input} from 'angular2/core';

import {Farm} from './../entities/farm';

@Component({
    selector: 'my-farm-detail',
    styles: [`
    table {
        border: solid 1px;
        border-spacing: 5px;
    }`],
    template: `
    <div *ngIf="farm">
        <h2>Fields of Farm {{farm.name}}</h2>
        <table *ngIf="farm.fields">
            <tbody>
            <tr>
                <th>Name</th>
                <th>Area</th>
                <th>Crop</th>
                <th>Lat</th>
                <th>Long</th>
                <th></th>
            </tr>
            <tr *ngFor="#farm of farm.fields"
                [class.selected]="farm === selectedFarm"
                (click)="onSelect(farm)">
                <th>{{farm.name}}</th>
                <th>{{farm.area}}</th>
                <th>{{farm.crop}}</th>
                <th>{{farm.lat}}</th>
                <th>{{farm.long}}</th>
                <td><input type="button" value="Del"></td>
            </tr>
            </tbody>
        </table>
    </div>`,
})

export class FarmDetailComponent {
    @Input()
    farm: Farm;
}
